--
-- Database: `inventory_tracker`
--
USE `inventory_tracker`;

INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Boneless Chuck', 5.99, 'Sold by the pound', 'National Beef');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Top Sirloin', 8.99, 'Sold by the pound', 'National Beef');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Pork Loin', 2.99, 'Sold by the pound', 'John Morrell');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Pork Shoulder', 1.49, 'Sold by the pound', 'John Morrell');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Halibut Cheeks', 19.99, 'Sold by the pound', 'Pacific Seafood');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Dover Sole', 3.99, 'Sold by the pound', 'Pacific Seafood');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Sockeye Salmon', 10.99, 'Sold by the pound', 'Pacific Seafood');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Burritos', 6.99, '32 oz', 'El Monterey');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Meatballs', 3.99, '16 oz', 'Armour');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Buffalo Wings', 8.99, '32 oz', 'Foster Farms');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Hot Dogs', 4.99, '12 oz', 'Hebrew National');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Bacon', 2.99, '16 oz', 'Oscar Meyer');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Corn Flakes', 4.99, '40oz', 'Kellogg''s');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Crispix', 4.99, '40oz', 'Kellogg''s');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Raisin Bran', 4.99, '40oz', 'Kellogg''s');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Lettuce', 1.79, 'Sold by the each', 'Merril Farms');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Broccoli', 0.79, 'Sold by the pound', 'Merril Farms');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Celery', 1.99, 'Sold by the each - 3 lb bag', 'R.C. Farms');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Artichoke', 3.99, 'Sold by the each', 'Merril Farms');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Spinach', 0.79, 'Sold by the pound', 'R.C. Farms');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Tomato', 1.99, 'Sold by the pound', 'R.C. Farms');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Organic Parsley', 1.49, 'Sold by the bunch', 'Mission Ranches');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Organic Bell Pepper', 1.49, 'Sold by the each', 'Mission Ranches');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Organic Carrot', 1.49, 'Sold by the pound', 'Mission Ranches');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Organic Bok Choy', 1.99, 'Sold by the pound', 'Mission Ranches');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Carrot', 1.49, 'Sold by the pound', 'Mission Ranches');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Donuts', 2.99, '6 pack - 13.5 oz', 'Franz');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Bread', 2.49, '24 oz', 'Orowheat');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Roast Beef', 7.49, 'Sold by the pound.', 'Hormel');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Havarti', 6.99, 'Sold by the pound.', 'Cady Creek');
INSERT INTO PRODUCT (name, description, manufacturer) VALUES ('Coffee Beans','Not for retail sale', 'Craven''s ' 'Coffee');
INSERT INTO PRODUCT (name, description, manufacturer) VALUES ('Vanilla Flavoring','Not for retail sale', 'DaVinci');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('2% Milk', 1.99, '1 gallon', 'Darigold');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Skim Milk', 1.99, '32 oz', 'Darigold');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Azaleas', 6.99, '1 potted plant', 'Roses & More');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Mylar Balloon', 8.99, '1 balloon', 'Roses & More');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Lemon Tart', 2.99, '2 oz', 'Galaxy Deserts');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Chocolate Cupcakes', 3.99, '13.5 oz', 'Lofthouse');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Toothpaste', 3.99, '6 oz', 'Colgate');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Mouthwash', 5.99, '18 oz', 'ACT');
INSERT INTO PRODUCT (name, price, description, manufacturer) VALUES ('Band Aids', 4.99, 'Pack of 30', 'Johnson & Johnson');
INSERT INTO PRODUCT (name, description, manufacturer) VALUES ('Rice','Not for retail sale', 'Nikishi');
INSERT INTO PRODUCT (name, description, manufacturer) VALUES ('Tuna','Not for retail sale', 'Pacific Seafood');

-- SUPPLIER
INSERT INTO SUPPLIER (name) VALUES ('Food Services of America');
INSERT INTO SUPPLIER (name) VALUES ('Odom');
INSERT INTO SUPPLIER (name) VALUES ('URM');
INSERT INTO SUPPLIER (name) VALUES ('Franz');
INSERT INTO SUPPLIER (name) VALUES ('Orowheat');
INSERT INTO SUPPLIER (name) VALUES ('Acosta');
INSERT INTO SUPPLIER (name) VALUES ('Charlie''s ' 'Produce');
INSERT INTO SUPPLIER (name) VALUES ('Pacific Seafood');
INSERT INTO SUPPLIER (name) VALUES ('Roses & More');

-- CATEGORY
INSERT INTO CATEGORY (name) VALUES ('Meat'); 
INSERT INTO CATEGORY (name) VALUES ('Seafood'); 
INSERT INTO CATEGORY (name) VALUES ('Frozen Meat'); 
INSERT INTO CATEGORY (name) VALUES ('Meat Deli'); 
INSERT INTO CATEGORY (name) VALUES ('Service Deli'); 
INSERT INTO CATEGORY (name) VALUES ('Espresso'); 
INSERT INTO CATEGORY (name) VALUES ('Sushi'); 
INSERT INTO CATEGORY (name) VALUES ('Produce'); 
INSERT INTO CATEGORY (name) VALUES ('Organics'); 
INSERT INTO CATEGORY (name) VALUES ('Floral'); 
INSERT INTO CATEGORY (name) VALUES ('Grocery'); 
INSERT INTO CATEGORY (name) VALUES ('Dairy'); 
INSERT INTO CATEGORY (name) VALUES ('General Merchandise'); 
INSERT INTO CATEGORY (name) VALUES ('Bakery'); 

-- CATEGORY_CATEGORY
INSERT INTO CATEGORY_CATEGORY (parentID, childID) VALUES (1, 2);
INSERT INTO CATEGORY_CATEGORY (parentID, childID) VALUES (1, 3);
INSERT INTO CATEGORY_CATEGORY (parentID, childID) VALUES (1, 4);
INSERT INTO CATEGORY_CATEGORY (parentID, childID) VALUES (5, 6);
INSERT INTO CATEGORY_CATEGORY (parentID, childID) VALUES (5, 7);
INSERT INTO CATEGORY_CATEGORY (parentID, childID) VALUES (8, 9);
INSERT INTO CATEGORY_CATEGORY (parentID, childID) VALUES (11, 12);
INSERT INTO CATEGORY_CATEGORY (parentID, childID) VALUES (11, 13);

-- PRODUCT_CATEGORY
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (1, 1);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (2, 1);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (3, 1);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (4, 1);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (5, 2);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (6, 2);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (7, 2);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (8, 3);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (9, 3);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (10, 3);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (11, 4);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (12, 4);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (13, 11);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (14, 11);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (15, 11);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (16, 8);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (17, 8);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (18, 8);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (19, 8);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (20, 8);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (21, 8);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (22, 9);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (23, 9);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (24, 9);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (25, 9);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (26, 8);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (27, 11);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (28, 11);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (29, 5);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (30, 5);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (31, 6);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (32, 6);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (33, 12);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (34, 12);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (35, 10);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (36, 10);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (37, 14);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (38, 14);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (39, 13);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (40, 13);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (41, 13);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (42, 7);
INSERT INTO PRODUCT_CATEGORY (productID, categoryID) VALUES (43, 7);

