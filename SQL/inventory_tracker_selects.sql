--
-- Database: `inventory_tracker`
--
USE `inventory_tracker`;

--
-- Get available categories for app drop downs
--
SELECT categoryID, name FROM CATEGORY;

--
-- Get available orders for order line item add form
--
SELECT orderID, orderDate FROM PURCHASE_ORDER;

--
-- Get available suppliers for app drop downs
--
SELECT supplierID, name FROM SUPPLIER;

--
-- Get inventory for all products from all suppliers
--
SELECT p.productID, p.name, p.description, p.manufacturer, p.price, SUM(i.quantity) AS quantity
	FROM PRODUCT AS p JOIN INVENTORY AS i ON i.productID = p.productID
	GROUP BY p.productID, p.name, p.description, p.manufacturer, p.price;

--
-- Get all top level categories for displaying the category tree
--
SELECT c.categoryID, c.name FROM CATEGORY c LEFT JOIN CATEGORY_CATEGORY p ON p.childID = c.categoryID WHERE p.parentID IS NULL;

--
-- Get category children for a passed in category ID.  Used in the recursive function that builds the category tree
--
SET @parent_category = 1;
SELECT p.parentID, c.categoryID, c.name FROM CATEGORY c INNER JOIN CATEGORY_CATEGORY p ON p.childID = c.categoryID WHERE p.parentID = @parent_category;

--
-- Get products to display in the Product view
--
SELECT p.productID, p.name, p.description, p.manufacturer, p.price, c.name as categoryName 
	FROM PRODUCT p LEFT JOIN PRODUCT_CATEGORY pc ON pc.productID = p.productID 
	LEFT JOIN CATEGORY c ON c.categoryID = pc.categoryID;

--
-- Get products based on search box entry for product name
--
SET @search_term = '';
SELECT p.productID, p.name, p.description, p.manufacturer,p.price , c.name as categoryName
	FROM PRODUCT p LEFT JOIN PRODUCT_CATEGORY pc ON pc.productID = p.productID 
	LEFT JOIN CATEGORY c ON c.categoryID = pc.categoryID WHERE p.name LIKE CONCAT('%', @search_term, '%');


--
-- Get orders to display in the Orders view
--
SELECT orderID, orderDate, total, status,receivedDate FROM PURCHASE_ORDER;

--
-- Get purchase order lines for each order
--
SET @order_number = 1;
SELECT po.orderID, po.status, pod.productID, pod.qtyOrdered, pod.lineItemPrice 
	FROM PURCHASE_ORDER po LEFT JOIN PURCHASE_ORDER_DETAILS pod ON po.orderID = pod.orderID 
    WHERE po.orderID = @order_number;
