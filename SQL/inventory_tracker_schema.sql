--
-- Database: `inventory_tracker`
--
CREATE DATABASE `inventory_tracker`;
USE `inventory_tracker`;

-- --------------------------------------------------------

--
-- Table structure for table `CATEGORY`
--

CREATE TABLE `CATEGORY` (
  `categoryID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  CONSTRAINT pk_category PRIMARY KEY (`categoryID`)
);

-- --------------------------------------------------------

--
-- Table structure for table `PRODUCT`
--

CREATE TABLE `PRODUCT` (
  `productID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `manufacturer` varchar(30) DEFAULT NULL,
  CONSTRAINT pk_product PRIMARY KEY (`productID`)
);

-- --------------------------------------------------------

--
-- Table structure for table `SUPPLIER`
--

CREATE TABLE `SUPPLIER` (
  `supplierID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  CONSTRAINT pk_supplier PRIMARY KEY (`supplierID`)
);

-- --------------------------------------------------------

--
-- Table structure for table `INVENTORY`
--

CREATE TABLE `INVENTORY` (
  `productID` int(11) NOT NULL,
  `supplierID` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  CONSTRAINT pk_inventory PRIMARY KEY (`productID`,`supplierID`),
  CONSTRAINT  `fk_inventory_product` FOREIGN KEY ( `productID` )
    REFERENCES  `inventory_tracker`.`PRODUCT` (`productID`)
	ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT  `fk_inventory_supplier` FOREIGN KEY (`supplierID`)
    REFERENCES  `inventory_tracker`.`SUPPLIER` (`supplierID`)
	ON DELETE RESTRICT ON UPDATE CASCADE
);

-- --------------------------------------------------------

--
-- Table structure for table `PURCHASE_ORDER`
--

CREATE TABLE `PURCHASE_ORDER` (
  `orderID` int(11) NOT NULL AUTO_INCREMENT,
  `orderDate` date DEFAULT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `receivedDate` date DEFAULT NULL,
  CONSTRAINT pk_purchase_order PRIMARY KEY (`orderID`)
);

-- --------------------------------------------------------

--
-- Table structure for table `PURCHASE_ORDER_DETAILS`
--

CREATE TABLE `PURCHASE_ORDER_DETAILS` (
  `orderID` int(11) NOT NULL,
  `productID` int(11) NOT NULL,
  `qtyOrdered` int(11) DEFAULT NULL,
  `lineItemPrice` decimal(10,2) DEFAULT NULL,
  CONSTRAINT pk_purchase_order_details PRIMARY KEY (`orderID`, `productID`),
  CONSTRAINT  `fk_details_purchase_order` FOREIGN KEY (`orderID`)
	REFERENCES `inventory_tracker`.`PURCHASE_ORDER` (`orderID`)
	ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT  `fk_details_product` FOREIGN KEY (`productID`)
	REFERENCES `inventory_tracker`.`PRODUCT` (`productID`)
	ON DELETE RESTRICT ON UPDATE CASCADE
);

-- --------------------------------------------------------

--
-- Table structure for table `PRODUCT_CATEGORY`
--

CREATE TABLE `PRODUCT_CATEGORY` (
  `productID` int(11) NOT NULL,
  `categoryID` int(11) NOT NULL,
  CONSTRAINT pk_inventory PRIMARY KEY (`productID`,`categoryID`),
  CONSTRAINT  `fk_product_category_product` FOREIGN KEY ( `productID` ) 
	REFERENCES  `inventory_tracker`.`PRODUCT` ( `productID` ) 
	ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT  `fk_product_category_category` FOREIGN KEY ( `categoryID` ) 
	REFERENCES  `inventory_tracker`.`CATEGORY` ( `categoryID` ) 
	ON DELETE RESTRICT ON UPDATE CASCADE
);

-- --------------------------------------------------------

--
-- Table structure for table `CATEGORY_CATEGORY`
--

CREATE TABLE `CATEGORY_CATEGORY` (
  `parentID` int(11) NOT NULL,
  `childID` int(11) NOT NULL,
  CONSTRAINT pk_inventory PRIMARY KEY (`parentID`,`childID`),
  CONSTRAINT  `fk_category_parent` FOREIGN KEY ( `parentID` ) 
	REFERENCES  `inventory_tracker`.`CATEGORY` ( `categoryID` ) 
	ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT  `fk_category_child` FOREIGN KEY ( `childID` ) 
	REFERENCES  `inventory_tracker`.`CATEGORY` ( `categoryID` ) 
	ON DELETE RESTRICT ON UPDATE CASCADE
);
