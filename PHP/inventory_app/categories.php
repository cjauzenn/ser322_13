<?php 
$title="SER 322 - Team 13: Inventory Tracker";
$description="Inventory Tracking system including purchase orders for product inventory";
$page_title="Categories";
include("includes/db.php");
include("includes/save.php");

require("includes/header.php");
?>
	<div class="row">
		<div class="twelve columns">
			<a class="button button-primary" href="add_category.php">Add Category</a>
		</div>
	</div>
	<?php
		$categories = get_category_tree(0);
		include("includes/category_view.php");
	?>
<?php
require("includes/footer.php");
?> 