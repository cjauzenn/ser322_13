<?php 
$title="SER 322 - Team 13: Inventory Tracker";
$description="Inventory Tracking system including purchase orders for product inventory";
$page_title="Products";
include("includes/db.php");
include("includes/save.php");

require("includes/header.php");
?>
	<?php
		$products = search_products($_GET['name']);
		include("includes/products_list.php");
	?>
<?php
require("includes/footer.php");
?> 