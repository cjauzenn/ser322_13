<?php 
$title="SER 322 - Team 13: Inventory Tracker";
$description="Inventory Tracking system including purchase orders for product inventory";
$page_title="Purchase Orders";
include("includes/db.php");
include("includes/save.php");

require("includes/header.php");
?>
	<div class="row">
		<div class="twelve columns">
			<a class="button button-primary" href="add_PO.php">Add Order</a>
			<a class="button button-primary" href="add_PO_line.php">Add Order Line Item</a>
                        <a class="button button-primary" href="search_po_line.php">View PO Line Item</a>
                </div>
        </div>                     

	<?php
		$purchase_orders = get_purchase_Orders();
		include("includes/purchase_orders_list.php");
	?>
<?php
require("includes/footer.php");
?> 