<?php 
$title="SER 322 - Team 13: Inventory Tracker";
$description="Inventory Tracking system including purchase orders for product inventory";
$page_title="Inventory Overview";
include("includes/db.php");
include("includes/save.php");

require("includes/header.php");
?>
	<div class="row">
		<div class="twelve columns">
			<a class="button button-primary" href="add_inventory.php">Add Inventory</a>
		</div>
	</div>

	<?php
		$inventory = get_inventory();
		include("includes/inventory_list.php");
	?>

<?php
require("includes/footer.php");
?> 