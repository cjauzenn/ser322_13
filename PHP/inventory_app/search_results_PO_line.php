<?php 
$title="SER 322 - Team 13: Inventory Tracker";
$description="Inventory Tracking system including purchase orders for product inventory";
$page_title="Purchase Order Line Item";
include("includes/db.php");
include("includes/save.php");

require("includes/header.php");
?>
	<?php
		$products = search_PO_line($_GET['orderID']);
		include("includes/PO_line_list.php");
	?>
<?php
require("includes/footer.php");
?> 