<?php 
$title="SER 322 - Team 13: Add Purchase Order Line Item";
$description="Inventory Tracking system including purchase orders for product inventory";
$page_title="Add Purchase Order Line Item";

include("includes/db.php");

require("includes/header.php");
?>  
	<?php include("includes/add_PO_line_form.php"); ?>

<?php
require("includes/footer.php");
?> 