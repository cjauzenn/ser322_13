<?php 
$title="SER 322 - Team 13: Inventory Tracker";
$description="Inventory Tracking system including purchase orders for product inventory";
$page_title="Products";
include("includes/db.php");
include("includes/save.php");

require("includes/header.php");
?>
	<div class="row">
		<div class="twelve columns">
			<a class="button button-primary" href="search_product.php">Search Products</a>
			<a class="button button-primary" href="add_product.php">Add Products</a>
		</div>
	</div>

	<?php
		$products = get_products();
		include("includes/products_list.php");
	?>

<?php
require("includes/footer.php");
?> 