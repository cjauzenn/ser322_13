<?php
if ($_POST) {
	if (!empty($_POST['add_product'])) {
		//add call to save_product() function
		if (save_product($_POST))
			$success_alert = "Product saved!";
	}
	if (!empty($_POST['add_inventory'])) {
		if (save_inventory($_POST))
			$success_alert = "Inventory saved!";
	}
	if (!empty($_POST['add_category'])) {
		if (save_category($_POST))
			$success_alert = "Category saved!";
	}
	if (!empty($_POST['add_supplier'])) {
		if (save_supplier($_POST))
			$success_alert = "Supplier saved!";
	}
	if (!empty($_POST['add_PO'])) {
		if (save_PO($_POST))
			$success_alert = "Purchase Order saved!";
	}
	if (!empty($_POST['add_PO_line'])) {
		if (save_PO_line($_POST))
			$success_alert = "Purchase Order Line Item saved!";
	}
}
?>