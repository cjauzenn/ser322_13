<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php echo $title ?></title>
		<meta name="description" content="<?php echo $description ?>"> 
		<meta name="author" content="SER 322 - Team 13">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

		<link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
		
		<!-- boilerplate stylesheets -->
        <link rel="stylesheet" href="css/normalize.css">
		<link rel="stylesheet" href="css/skeleton.css">
        <link rel="stylesheet" href="css/main.css">
		
		<!-- custom styles -->
		<link rel="stylesheet" href="css/style.css">

        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
		<div class="header">
			<div class="container">
				<div class="row menu">
					<ul>
						<li>
							<a href="index.php">Inventory</a>
						</li>
						<li>
							<a href="categories.php">Categories</a>
						</li>
						<li>
							<a href="products.php">Products</a>
						</li>
						<li>
							<a href="suppliers.php">Suppliers</a>
						</li>
						<li>
							<a href="purchase_orders.php">Purchase Orders</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="container">
		<h4>
			<?php echo $page_title;?>
		</h4>
<?php
	if (isset($success_alert)) {
		echo '<div class="row"><div class="one-half column success">'.
			'<span class="closebtn" onclick="this.parentElement.style.display=\'none\'">&times;</span>'.
			$success_alert.'</div></div>';
	}
	if (isset($error_alert)) {
		echo '<div class="row"><div class="one-half column alert">'.
			'<span class="closebtn" onclick="this.parentElement.style.display=\'none\'">&times;</span>'.
			$error_alert.'</div></div>';
	}
//error debugging
error_reporting(E_ALL); ini_set('display_errors', 1); 
?>