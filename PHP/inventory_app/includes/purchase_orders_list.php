<div class="row">
	<div class="nine columns">&nbsp;</div>

</div>
<div class="row">
	<div class="twelve columns" >
		<table class="u-full-width">
			<thead>
				<tr>
					<th>Order ID</th>
					<th>Order Date</th>
					<th>Total</th>
					<th>Status</th>
					<th>Received Date</th>
				</tr>
			</thead>
                <tbody>
                    <?php
                        foreach ($purchase_orders as $item) {
                            echo '<tr><td><a href="search_results_PO_line.php?orderID='.$item['orderID'].'">'.$item['orderID'].'</a></td>'.
                                '<td>'.$item['orderDate'].'</td>'.
                                '<td>'.$item['total'].'</td>'.
                                '<td>'.$item['status'].'</td>'.
                                '<td>'.$item['receivedDate'].'</td>'.
                                '</tr>';
                        }
                    ?>
                </tbody>
		</table>
	</div>
</div>