<div class="row">
	<div class="twelve columns" >
		<table class="u-full-width">
			<thead>
				<tr>
					<th>Product ID</th>
					<th>Product Name</th>
					<th>Category</th>
					<th>Description</th>
					<th>Manufacturer</th>
					<th>Price</th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach ($products as $item) {
						echo '<tr><td>'.$item['productID'].'</td>'.
							'<td>'.$item['name'].'</td>'.
							'<td>'.$item['categoryName'].'</td>'.
							'<td>'.$item['description'].'</td>'.
							'<td>'.$item['manufacturer'].'</td>'.
							'<td>'.$item['price'].'</td>'.
							'</tr>';
					}
				?>
			</tbody>
		</table>
	</div>
</div>