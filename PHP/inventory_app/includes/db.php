<?php
//define database constants
DEFINE ('DB_HOSTNAME', 'localhost');
DEFINE ('DB_DATABASE', 'inventory_tracker');
DEFINE ('DB_USERNAME', 'root');
DEFINE ('DB_PASSWORD', 'P@ssw0rd1');

//connect to db
function get_db() {
    $db = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
    if(!$db) die('Unable to connect to MySQL: ' . mysqli_error($db));
    return $db;
}

function close_db($db) {
	mysqli_close($db);
}

function select_from_db($query) {
	$db = get_db();
	$result = mysqli_query($db, $query) or die('Error querying database:' . mysqli_error($db));

	$results = array();
	while ($row = mysqli_fetch_array($result)) {
		$results[] = $row;
	}
	
	close_db($db);
	return $results;
}

function get_categories() {
	$query = 'SELECT categoryID, name FROM CATEGORY';
	return select_from_db($query);
}

function get_orderIDs() {
	$query = 'SELECT orderID, orderDate FROM PURCHASE_ORDER';
	return select_from_db($query);
}

function get_category_tree($categoryID) {
	$query = 'SELECT c.categoryID, c.name FROM CATEGORY c LEFT JOIN CATEGORY_CATEGORY p ON p.childID = c.categoryID WHERE p.parentID IS NULL';
	if ($categoryID > 0) {
		$query = 'SELECT p.parentID, c.categoryID, c.name FROM CATEGORY c INNER JOIN CATEGORY_CATEGORY p ON p.childID = c.categoryID WHERE p.parentID = '.$categoryID;
	}
	$categories = select_from_db($query);

	foreach ($categories as $key => $item) {
		$categories[$key]['children'] = get_category_tree($item['categoryID']);
	}

	return $categories;
}

function get_inventory() {
	$query = 'SELECT p.productID, p.name, p.description, p.manufacturer, p.price, SUM(i.quantity) AS quantity '.
		'FROM PRODUCT AS p JOIN INVENTORY AS i ON i.productID = p.productID '.
		'GROUP BY p.productID, p.name, p.description, p.manufacturer, p.price';
	return select_from_db($query);
}

function get_suppliers() {
	$query = 'SELECT supplierID, name FROM SUPPLIER';
	return select_from_db($query);
}

function get_purchase_orders() {
	$query = 'SELECT orderID, orderDate, total, status,receivedDate FROM PURCHASE_ORDER';
	return select_from_db($query);
}

function get_products() {
	$query = 'SELECT p.productID, p.name, p.description, p.manufacturer, p.price, c.name as categoryName '.
		'FROM PRODUCT p LEFT JOIN PRODUCT_CATEGORY pc ON pc.productID = p.productID '.
		'LEFT JOIN CATEGORY c ON c.categoryID = pc.categoryID';
	return select_from_db($query);
}

function search_products($name) {
	$query = 'SELECT p.productID, p.name, p.description, p.manufacturer,p.price , c.name as categoryName
	FROM PRODUCT p LEFT JOIN PRODUCT_CATEGORY pc ON pc.productID = p.productID 
		LEFT JOIN CATEGORY c ON c.categoryID = pc.categoryID WHERE p.name LIKE \'%'.$name.'%\'';
	return select_from_db($query);
}

function save_product($args) {
	$db = get_db();
	$query = 'INSERT INTO PRODUCT(name, price, manufacturer, description) VALUES (\''.
		$args['name'].'\', '.
		$args['price'].', \''.
		$args['manufacturer'].'\', \''.
		$args['description'].'\');'.
		'INSERT INTO PRODUCT_CATEGORY(categoryID, productID) SELECT '.
			$args['category'].', productID FROM PRODUCT WHERE name = \''.
			$args['name'].'\' ORDER BY productID DESC LIMIT 1';
		
	mysqli_multi_query($db, $query) or die('Error querying database:' . mysqli_error($db));

	close_db($db);
	return true;
}

function save_inventory($args) {
	$db = get_db();
	$query = 'INSERT INTO INVENTORY(productID, quantity, supplierID) VALUES ('.
		$args['product'].', '.
		$args['quantity'].', '.
		$args['supplier'].') '.
		'ON DUPLICATE KEY UPDATE '.
		'quantity = quantity + '.$args['quantity'];
	mysqli_query($db, $query) or die('Error querying database:' . mysqli_error($db));

	close_db($db);
	return true;
}

function save_category($args) {
	$db = get_db();
	$query = 'INSERT INTO CATEGORY(name) VALUES(\''.$args['name'].'\');';
	if ($args['parent']>0) {
		$query .= 'INSERT INTO CATEGORY_CATEGORY(parentID, childID) SELECT '.
			$args['parent'].', categoryID FROM CATEGORY WHERE name = \''.
			$args['name'].'\' ORDER BY categoryID DESC LIMIT 1';
	}
	
	mysqli_multi_query($db, $query) or die('Error querying database:' . mysqli_error($db));

	close_db($db);
	return true;
}

function save_supplier($args) {
	$db = get_db();
	$query = 'INSERT INTO SUPPLIER(name) VALUES(\''.$args['name'].'\');';
	
	mysqli_query($db, $query) or die('Error querying database:' . mysqli_error($db));

	close_db($db);
	return true;
}

function save_PO($args) {
	$db = get_db();
	$query = 'INSERT INTO PURCHASE_ORDER (orderDate, status) VALUES(\''.
		$args['orderDate'].'\', \''.
		$args['status'].'\') ';
	
	if (!empty($args['receivedDate'])) {
		$query = 'INSERT INTO PURCHASE_ORDER (orderDate, status, receivedDate) VALUES(\''.
			$args['orderDate'].'\', \''.
			$args['status'].'\', \''.
			$args['receivedDate'].'\') ';
	}

	mysqli_query($db, $query) or die('Error querying database:' . mysqli_error($db));

	close_db($db);
	return true;
}

function save_PO_line($args) {
	$db = get_db();
	$query = 'INSERT INTO PURCHASE_ORDER_DETAILS (orderID, productID, qtyOrdered, lineItemPrice)VALUES('.
		$args['orderID'].', '.
		$args['product'].', '.
		$args['qtyOrdered'].', '.
		$args['lineItemPrice'].') '.
		'ON DUPLICATE KEY UPDATE '.
		'qtyOrdered = qtyOrdered + '.$args['qtyOrdered'].', lineItemPrice = '.$args['lineItemPrice'].';'.
		'UPDATE PURCHASE_ORDER SET total = COALESCE(total, 0) + '.$args['qtyOrdered'] * $args['lineItemPrice'].' '.
		'WHERE orderID = '.$args['orderID'];
	
	mysqli_multi_query($db, $query) or die('Error querying database:' . mysqli_error($db));

	close_db($db);
	return true;
}

function search_PO_line($orderID) {
	$query = 'SELECT po.orderID, po.status, pod.productID, pod.qtyOrdered, pod.lineItemPrice '.
		'FROM PURCHASE_ORDER po LEFT JOIN PURCHASE_ORDER_DETAILS pod ON po.orderID = pod.orderID '.
        'WHERE po.orderID = '.$orderID;
	return select_from_db($query);
}

?>