<form action="purchase_orders.php" method="post">
  <div class="row">
  	<div class="six columns">
	  <label for="orderDate">Order Date</label>
	  <input class="u-full-width" type="date" placeholder='00/00/0000' name="orderDate">
	</div>
  </div>
  <div class="row">
	<div class="six columns" type="varchar(20)">
	  <label for="status">Status</label>
	  <p> Order Status <select name="status">
		<option value="">Select...</option>
		<option value="Received">Received</option>
		<option value="Not Received">Not Received</option>
		</select>
	  </p>
	</div>
  </div>
  <div class="row">
  	<div class="six columns">
	  <label for="receivedDate">Order Received Date</label>
	  <input class="u-full-width" type="date" placeholder='00/00/0000' name="receivedDate">
	</div>
  </div>
  <div class="row">
	<div class="ten columns">&nbsp;</div>
	<div class="two columns">
		<input class="button-primary" type="submit" value="Save" name="add_PO">
	</div>
  </div>
</form>
