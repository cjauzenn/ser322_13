<div class="row">
	<div class="nine columns">&nbsp;</div>

</div>
<div class="row">
	<div class="twelve columns" >
		<table class="u-full-width">
			<thead>
				<tr>
					<th>Supplier ID</th>
					<th>Supplier Name</th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach (get_suppliers() as $item) {
						echo '<tr><td>'.$item['supplierID'].'</td>'.
							'<td>'.$item['name'].'</td>'.
							'</tr>';
					}
				?>
			</tbody>
		</table>
	</div>
</div>