<div class="row">
	<div class="nine columns">&nbsp;</div>

</div>
<div class="row">
	<div class="twelve columns" >
		<table class="u-full-width">
			<thead>
				<tr>
                                        <th>Order ID</th>
					<th>Product ID</th>
					<th>Quantity Ordered</th>
                                        <th>Item Price</th>
                                        <th>Status</th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach ($products as $item) {
						echo '<tr><td>'.$item['orderID'].'</td>'.
                                                        '<td>'.$item['productID'].'</td>'.  
							'<td>'.$item['qtyOrdered'].'</td>'.
                                                        '<td>'.$item['lineItemPrice'].'</td>'.
                                                        '<td>'.$item['status'].'</td>'.
							'</tr>';
					}
				?>
			</tbody>
		</table>
	</div>
</div>
