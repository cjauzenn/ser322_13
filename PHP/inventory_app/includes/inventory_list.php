<div class="row">
	<div class="twelve columns" >
		<table class="u-full-width">
			<thead>
				<tr>
					<th>Product ID</th>
					<th>Product Name</th>
					<th>Description</th>
					<th>Manufacturer</th>
					<th>Price</th>
					<th>Quantity</th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach ($inventory as $item) {
						echo '<tr><td>'.$item['productID'].'</td>'.
							'<td>'.$item['name'].'</td>'.
							'<td>'.$item['description'].'</td>'.
							'<td>'.$item['manufacturer'].'</td>'.
							'<td>'.$item['price'].'</td>'.
							'<td>'.$item['quantity'].'</td></tr>';
					}
				?>
			</tbody>
		</table>
	</div>
</div>