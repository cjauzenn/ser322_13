<form action="index.php" method="post">
  <div class="row">
	<div class="six columns">
	  <label for="product">Product</label>
	  <select class="u-full-width" name="product">
		<?php
			foreach(get_products() as $product) {
				echo '<option value="'.$product['productID'].'">'.$product['name'].'</option>';
			}
		?>
	  </select>
	</div>
	<div class="three columns">
	  <label for="quantity">Quantity</label>
	  <input class="u-full-width" type="number" placeholder="0" name="quantity">
	</div>
  </div>
  <div class="row">
	<div class="six columns">
	  <label for="supplier">Supplier</label>
	  <select class="u-full-width" name="supplier">
		<?php
			foreach(get_suppliers() as $supplier) {
				echo '<option value="'.$supplier['supplierID'].'">'.$supplier['name'].'</option>';
			}
		?>
	  </select>
	</div>
  </div>
  <div class="row">
	<div class="ten columns">&nbsp;</div>
	<div class="two columns">
		<input class="button-primary" type="submit" value="Save" name="add_inventory">
	</div>
  </div>
</form>