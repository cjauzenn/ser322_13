<div class="row">
	<div class="twelve columns" >
		<div class="categories">
			<ul>
			<?php
				function print_list($list) {
					$result = '<ul>';
					foreach($list as $item) {
						$result .= '<li>'.$item['name'];
						if (!empty($item['children'])) {
							$result .= print_list($item['children']);
						}
						$result .= '</li>';
					}
					$result .= '</ul>';

					return $result;
				}

				echo print_list($categories);
			?>
			</ul>
		</div>
	</div>
</div>