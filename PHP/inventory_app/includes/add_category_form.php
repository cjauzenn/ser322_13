<form action="categories.php" method="post">
  <div class="row">
	<div class="six columns">
	  <label for="category">Parent</label>
	  <select class="u-full-width" name="parent">
		<option value="0">No Parent</option>
		<?php
			foreach(get_categories() as $category) {
				echo '<option value="'.$category['categoryID'].'">'.$category['name'].'</option>';
			}
		?>
	  </select>
	</div>
	<div class="six columns">
	  <label for="name">Name</label>
	  <input class="u-full-width" type="text" placeholder="Category Name" name="name">
	</div>
  </div>
  <div class="row">
	<div class="ten columns">&nbsp;</div>
	<div class="two columns">
		<input class="button-primary" type="submit" value="Save" name="add_category">
	</div>
  </div>
</form>