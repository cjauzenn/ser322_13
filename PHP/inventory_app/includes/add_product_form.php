<form action="products.php" method="post">
  <div class="row">
  	<div class="six columns">
	  <label for="name">Product Name</label>
	  <input class="u-full-width" type="text" placeholder="Product Name" name="name">
	</div>
	<div class="six columns">
	  <label for="category">Category</label>
	  <select class="u-full-width" name="category">
		<?php
			foreach(get_categories() as $category) {
				echo '<option value="'.$category['categoryID'].'">'.$category['name'].'</option>';
			}
		?>
	  </select>
	</div>
  </div>
  <div class="row">
	<div class="six columns">
	  <label for="manufacturer">Manufacturer</label>
	  <input class="u-full-width" type="text" placeholder="Manufacturer" name="manufacturer">
	</div>
	<div class="two columns">
	  <label for="price">Price</label>
	  <input class="u-full-width" type="number" step="0.01" placeholder="0.00" name="price">
	</div>
  </div>
  <div class="row">
	<div class="twelve columns">
		<label for="description">Description</label>
		<textarea class="u-full-width" placeholder="Product description..." name="description"></textarea>
	</div>
  </div>
  <div class="row">
	<div class="ten columns">&nbsp;</div>
	<div class="two columns">
		<input class="button-primary" type="submit" value="Save" name="add_product">
	</div>
  </div>
</form>