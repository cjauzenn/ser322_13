<form action="purchase_orders.php" method="post">
  <div class="row">
  	<div class="two columns">
	  <label for="orderID">Order ID</label>
            <select class="u-full-width" name="orderID">
		<?php
			foreach(get_orderIDs() as $orders) {
				echo '<option value="'.$orders['orderID'].'">'.$orders['orderID'].'</option>';
			}
		?>
	  </select>
	</div>  
  </div>
  <div class="row">
  <div class="four columns">
  <label for="product">Product</label>
	  <select class="u-full-width" name="product">
		<?php
			foreach(get_products() as $products) {
				echo '<option value="'.$products['productID'].'">'.$products['name'].'</option>';
			}
		?>
	  </select>
  </div>
  </div>
  <div class="row">
   <div class="two columns">
	  <label for="qtyOrdered">Quantity Ordered</label>
	  <input class="u-full-width" type="number" placeholder="qty" name="qtyOrdered">
	</div>
	<div class="two columns">
	  <label for="lineItemPrice">Line Item Price</label>
	  <input class="u-full-width" type="number" step="0.01" placeholder="0.00" name="lineItemPrice">
	</div>
  </div>
  <div class="row">
	<div class="ten columns">&nbsp;</div>
	<div class="two columns">
		<input class="button-primary" type="submit" value="Save" name="add_PO_line">
	</div>
  </div>
</form>
